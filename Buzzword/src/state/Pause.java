package state;

import controller.HomeController;
import controller.MyTimer;
import controller.PlayController;
import javafx.animation.Timeline;
import javafx.scene.text.Text;

import java.io.IOException;

import static settings.AppPropertyType.PLAY_ICON;
import static settings.AppPropertyType.PLAY_TOOLTIP;

/**
 * Created by zhangxinyao on 11/23/16.
 */
public class Pause implements State{

    @Override
    public void controlPane(HomeController home) {

    }

    @Override
    public void controlPause(PlayController play) throws IOException {
        Text[][] word = play.getWord();
        for (int i=0;i<4;i++) {
            for (int j = 0; j < 4; j++) {
                word[i][j].setText("");
            }
        }
        play.setPauseButton(PLAY_ICON.toString(),PLAY_TOOLTIP.toString());
        play.setPlaying(false);
        play.getInput().setDisable(true);
        play.getGrid().setMouseTransparent(true);
        play.clearHighlight();
        MyTimer timer = play.getTimer();
        Timeline timeline = timer.getTimeline();
        timeline.pause();
        play.setPauseState(new Resume());
    }
}

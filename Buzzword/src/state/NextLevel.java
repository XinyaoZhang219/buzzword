package state;

import controller.HomeController;
import controller.PlayController;
import javafx.scene.layout.BorderPane;

import java.io.IOException;

import static settings.AppPropertyType.PLAY_ICON;
import static settings.AppPropertyType.PLAY_TOOLTIP;

/**
 * Created by zhangxinyao on 12/10/16.
 */
public class NextLevel implements State {
    @Override
    public void controlPane(HomeController home) {

    }

    @Override
    public void controlPause(PlayController play) throws IOException {
        play.setPauseButton(PLAY_ICON.toString(),PLAY_TOOLTIP.toString());
        play.getGamedata().setPlayLevel(play.getGamedata().getPlayLevel()+1);
        BorderPane grid = play.getGrid();
        grid.setMouseTransparent(false);
        play.handlePlay();
    }
}

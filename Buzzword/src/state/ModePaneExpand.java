package state;


import controller.HomeController;
import controller.PlayController;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 * Created by zhangxinyao on 11/12/16.
 * @author Xinyao Zhang
 */
public class ModePaneExpand implements State {


    @Override
    public void controlPane(HomeController home){
        VBox modePane = home.getModePane();
        Label[] mode = home.getMode();
        for (int i=0; i<mode.length; i++){
            modePane.getChildren().add(mode[i]);
        }
        home.setModePaneState(new ModePaneShrink());
    }

    @Override
    public void controlPause(PlayController play) {
    }


}

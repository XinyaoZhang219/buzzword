package state;

import controller.HomeController;
import controller.PlayController;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

/**
 * Created by zhangxinyao on 11/12/16.
 * @author Xinyao Zhang
 */
public class ModePaneShrink implements State {


    @Override
    public void controlPane(HomeController home){
        VBox modePane = home.getModePane();
        Button select = home.getSelect();
        modePane.getChildren().setAll(select);
        home.setModePaneState(new ModePaneExpand());
    }

    @Override
    public void controlPause(PlayController play) {

    }
}

package state;

import controller.HomeController;
import controller.MyTimer;
import controller.PlayController;
import data.GameData;
import javafx.animation.Timeline;
import javafx.scene.layout.BorderPane;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;

import java.io.IOException;

import static settings.AppPropertyType.*;

/**
 * Created by zhangxinyao on 12/10/16.
 */
public class EndWin implements State {
    PropertyManager propertyManager = PropertyManager.getManager();

    @Override
    public void controlPane(HomeController home) {

    }

    @Override
    public void controlPause(PlayController play) throws IOException {
        play.setPauseButton(NEXT_ICON.toString(),NEXT_TOOLTIP.toString());
        play.setPlaying(false);
        play.getInput().setDisable(true);
        BorderPane grid = play.getGrid();
        grid.setMouseTransparent(true);
        MyTimer timer = play.getTimer();
        Timeline timeline = timer.getTimeline();
        timeline.stop();
        play.setGuess("");
        play.clearHighlight();
        play.showAnswer();
        GameData gamedata=play.getGamedata();
        if (gamedata.getPlayLevel() > gamedata.getPasslevel()[gamedata.getCurrentMode().getParameter()]){
            AppMessageDialogSingleton dialogSingleton = AppMessageDialogSingleton.getSingleton();
            dialogSingleton.show(propertyManager.getPropertyValue(BEST_TITLE.toString()),
                    propertyManager.getPropertyValue(BEST_MESSAGE.toString()));
        }
        play.setPauseState(new NextLevel());

    }
}

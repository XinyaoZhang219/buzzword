package state;

import controller.HomeController;
import controller.PlayController;

import java.io.IOException;

/**
 * Created by zhangxinyao on 11/12/16.
 * @author Xinyao Zhang
 */
public interface State {

     void controlPane(HomeController home);
     void controlPause(PlayController play) throws IOException;

}

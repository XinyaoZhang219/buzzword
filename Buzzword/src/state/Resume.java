package state;


import controller.HomeController;
import controller.MyTimer;
import controller.PlayController;
import javafx.animation.Timeline;
import javafx.scene.text.Text;

import java.io.IOException;

import static settings.AppPropertyType.PAUSE_ICON;
import static settings.AppPropertyType.PAUSE_TOOLTIP;

/**
 * Created by zhangxinyao on 11/23/16.
 */
public class Resume implements State {
    @Override
    public void controlPane(HomeController home) {

    }

    @Override
    public void controlPause(PlayController play) throws IOException {
        Text[][] word = play.getWord();
        String[][] letter = play.getGamedata().getLetterGrid();
        for (int i=0;i<4;i++) {
            for (int j = 0; j < 4; j++) {
                word[i][j].setText(letter[i][j]);
            }
        }
        play.setPauseButton(PAUSE_ICON.toString(),PAUSE_TOOLTIP.toString());
        play.setPlaying(true);
        play.getInput().setDisable(false);
        play.getGrid().setMouseTransparent(false);
        play.highlight(play.getInput().getText().length());
        MyTimer timer = play.getTimer();
        Timeline timeline = timer.getTimeline();
        timeline.play();
        play.setPauseState(new Pause());
    }
}

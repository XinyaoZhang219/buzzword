package data;

import com.fasterxml.jackson.core.*;
import components.AppDataComponent;
import components.AppFileComponent;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by zhangxinyao on 11/9/16.
 * @author Xinyao Zhang
 */
public class GameDataFile implements AppFileComponent {
    private static final String NAME  = "name";
    private static final String PASSWORD  = "password";
    private static final String PASSLEVEL  = "passLevel";
    private static final String LASTCATEGORY  = "lastCategory";


    @Override
    public void saveData(AppDataComponent data, Path filePath) throws IOException, NullPointerException {
        GameData gamedata = (GameData) data;
        if (gamedata.getName() == null){
            throw new NullPointerException("No profile need to save");
        }
        JsonFactory jsonFactory = new JsonFactory();

        try (OutputStream out = Files.newOutputStream(filePath)) {

            JsonGenerator generator = jsonFactory.createGenerator(out, JsonEncoding.UTF8);

            generator.writeStartObject();

            generator.writeStringField(NAME, gamedata.getName());
            generator.writeStringField(PASSWORD, encode(gamedata.getPassword()));

            generator.writeFieldName(PASSLEVEL);
            generator.writeArray(gamedata.getPasslevel(), 0, 4);

            generator.writeStringField(LASTCATEGORY, gamedata.getCurrentMode().toString());
            generator.writeEndObject();

            generator.close();
        }
    }

    @Override
    public void loadData(AppDataComponent data, Path filePath) throws IOException {
        GameData gamedata = (GameData) data;
        JsonFactory jsonFactory = new JsonFactory();
        JsonParser jsonParser  = jsonFactory.createParser(Files.newInputStream(filePath));

        while (!jsonParser.isClosed()) {
            JsonToken token = jsonParser.nextToken();
            if (JsonToken.FIELD_NAME.equals(token)) {
                String fieldname = jsonParser.getCurrentName();
                switch (fieldname) {
                    case NAME:
                        jsonParser.nextToken();
                        gamedata.setName(jsonParser.getValueAsString());
                        break;
                    case PASSWORD:
                        jsonParser.nextToken();
                        gamedata.setPassword(jsonParser.getValueAsString());
                        break;
                    case PASSLEVEL:
                        jsonParser.nextToken();
                        jsonParser.nextToken();
                        for (int i=0; i<4; i++) {
                            gamedata.setPasslevel(i,jsonParser.getValueAsInt());
                            jsonParser.nextToken();
                        }
                        break;
                    case LASTCATEGORY:
                        jsonParser.nextToken();
                        String category = jsonParser.getValueAsString();
                        switch (category){
                            case "ENGLISH_DICTIONARY":
                                gamedata.setCurrentMode(Category.ENGLISH_DICTIONARY);break;
                            case "DUTCH":
                                gamedata.setCurrentMode(Category.DUTCH);break;
                            case "ITALIAN":
                                gamedata.setCurrentMode(Category.ITALIAN);break;
                            case "NORWEGIAN":
                                gamedata.setCurrentMode(Category.NORWEGIAN);break;
                        }
                        break;
                    default:
                        throw new JsonParseException(jsonParser, "Unable to load JSON data");
                }
            }
        }
    }

    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException {

    }
    private String encode(String password) {
        StringBuffer hexValue = new StringBuffer();
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            char[] charArray = password.toCharArray();
            byte[] byteArray = new byte[charArray.length];

            for (int i = 0; i < charArray.length; i++)
                byteArray[i] = (byte) charArray[i];
            byte[] md5Bytes = md5.digest(byteArray);
            for (int i = 0; i < md5Bytes.length; i++) {
                int val = ((int) md5Bytes[i]) & 0xff;
                if (val < 16)
                    hexValue.append("0");
                hexValue.append(Integer.toHexString(val));
            }
        }catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        }
        return hexValue.toString();

    }
}

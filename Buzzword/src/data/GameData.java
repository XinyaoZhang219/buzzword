package data;

import apptemplate.AppTemplate;
import components.AppDataComponent;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Iterator;

/**
 * Created by zhangxinyao on 11/9/16.
 * @author Xinyao Zhang
 */
public class GameData implements AppDataComponent{

    private AppTemplate appTemplate;
    private String name;
    private String password;
    private int[] passlevel;

    private Category currentMode;
    private int playLevel;
    private int targetPoint;
    private int totalPoint;
    private int totalscore;
    private int limitTime;
    private HashSet<String> guessedWord;

    private String[][]  letterGrid;
    private HashSet<String> allLetterCombinations;
    private HashSet<String> allTargetWords;

    private HashSet<String> EnglishWords;
    private HashSet<String> DutchWords;
    private HashSet<String> ItalianWords;
    private HashSet<String> NorwegianWords;

    public boolean logined = false;


    public GameData(AppTemplate app){
        appTemplate =app;
        loadAllWords();
    }



    public String getName(){ return name; }

    public String getPassword(){ return password;}

    public String[][] getLetterGrid(){  return letterGrid;}


    public void setName(String n){ name = n; }
    public void setPassword(String pw){ password = pw; }



    public void setupTargetWords(){
        initLetter();
        findWord();
        switch (currentMode){
            case ENGLISH_DICTIONARY:setAllTargetWords(EnglishWords);break;
            case DUTCH: setAllTargetWords(DutchWords);break;
            case ITALIAN: setAllTargetWords(ItalianWords);break;
            case NORWEGIAN: setAllTargetWords(NorwegianWords);break;
        }
      if (calculateTotalPoint()<60) {
          setupTargetWords();
      }else {
          calculateTargetScore();
//          for (String s:allTargetWords) {
//              System.out.println(s);
//          }
          totalscore=0;
          calculateLimitTime();
          guessedWord=new HashSet<>();
      }
    }
    private void setAllTargetWords(HashSet<String> words){
        Iterator it = allLetterCombinations.iterator();
        allTargetWords = new HashSet<>();
        while (it.hasNext()) {
            String s =  it.next().toString();
            if (words.contains(s)) {
                allTargetWords.add(s);
            }
        }
    }
    private void initLetter(){
        letterGrid = new String[4][4];
        for (int i=0; i<4; i++){
            for (int j =0; j<4;j++){
                String c ;
                int offset =   ((int)(Math.random()*27));
                if (offset == 26)
                    c = "";
                else {
                    char a ='a';
                    a += offset;
                    c = a+"";
                }

                letterGrid[i][j]=c;
            }

        }
    }

    private void findWord(){
        allLetterCombinations = new HashSet<>();
        boolean[][] isUsed = new boolean[4][4];

        //word length from 3
        for (int k=3;k<=16;k++) {
            //star search letterGrid[i][j]
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {

                    for (int a=0;a<4;a++){
                        for (int b=0;b<4;b++){
                            isUsed[a][b]=false;
                        }
                    }
                    String s = "";
                    findCombinations(k, letterGrid, i, j, s, isUsed);
                }
            }
        }
    }

    private void findCombinations(int length,String[][] letterGrid,int row,int col, String combination,boolean[][] used){
        if (letterGrid[row][col].equals("")){
            return;
        }else {
            combination += letterGrid[row][col];
            used[row][col] = true;
        }
        if (length <= 1){
            allLetterCombinations.add(combination);
            return;
        }
        if (row-1>=0 && !letterGrid[row-1][col].equals("") && !used[row - 1][col]) {
            findCombinations(length - 1, letterGrid, row - 1, col, combination,used);
            used[row-1][col]=false;
        }
        if (col+1<4 && !letterGrid[row][col+1].equals("")&& !used[row][col + 1]) {
            findCombinations(length - 1, letterGrid, row , col+1, combination,used);
            used[row][col+1]=false;
        }
        if (row+1<4 && !letterGrid[row+1][col].equals("") && !used[row + 1][col]) {
            findCombinations(length - 1, letterGrid, row + 1, col, combination,used);
            used[row+1][col]=false;
        }
        if (col-1>=0 && !letterGrid[row][col-1].equals("") && !used[row][col - 1]) {
            findCombinations(length - 1, letterGrid, row, col-1, combination,used);
            used[row][col-1]=false;
        }
    }

    private void calculateTargetScore(){
        targetPoint = 0;
        targetPoint = (int) (totalPoint * (playLevel * 0.1));

    }
    private int calculateTotalPoint(){
        totalPoint=0;
        for (Object allTargetWord : allTargetWords) {
            totalPoint += allTargetWord.toString().length();
        }
        return totalPoint;
    }
    private void calculateLimitTime(){
        limitTime=0;
        limitTime=targetPoint*3;
    }

    private void loadAllWords(){
        EnglishWords = new HashSet<>();
        loadCategoryWords(Category.ENGLISH_DICTIONARY,EnglishWords);
        DutchWords = new HashSet<>();
        loadCategoryWords(Category.DUTCH,DutchWords);
        ItalianWords = new HashSet<>();
        loadCategoryWords(Category.ITALIAN,ItalianWords);
        NorwegianWords = new HashSet<>();
        loadCategoryWords(Category.NORWEGIAN,NorwegianWords);

    }
    private void loadCategoryWords(Category mode,HashSet<String> word){
        URL wordsResource = getClass().getClassLoader().getResource("words/"+categoryToString(mode)+".txt");
        assert wordsResource != null;
        try {
            File file = Paths.get(wordsResource.toURI()).toFile();
            InputStreamReader read = new InputStreamReader(new FileInputStream(file));
            BufferedReader bufferedReader = new BufferedReader(read);
            String lineText;
            while((lineText = bufferedReader.readLine()) != null){
                if (lineText.length()>=3 && lineText.length()<=16)
                    word.add(lineText);
            }
            read.close();
        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void reset() {
        name = null;
        password = null;
        passlevel = new int[4];
        for (int i=0; i<4; i++){
            passlevel[i]=0;
        }
        currentMode=Category.ENGLISH_DICTIONARY;
        targetPoint = 0;
        totalscore = 0;
        guessedWord = new HashSet<>();

    }
    public void setCurrentMode(Category mode){ currentMode = mode; }

    public void setPasslevel(Category mode, int level){
       passlevel[mode.getParameter()] = level;
    }

    public void setPasslevel(int mode, int level){  passlevel[mode]=level;}

    public void setPlayLevel(int i){ playLevel=i; }
    public void setTotalScore(int i){ totalscore =i;}

    public int getPlayLevel(){  return playLevel; }

    public int[] getPasslevel(){    return passlevel; }

    public Category getCurrentMode(){   return currentMode; }

    public HashSet<String> getGuessedWord() { return guessedWord;}

    public int getTargetPoint(){ return targetPoint;}
    public int getTotalPoint(){ return totalPoint;}

    public int getTotalscore(){ return totalscore;}
    public int getLimitTime() {return limitTime;}
    public HashSet<String> getAllTargetWords(){ return allTargetWords;}
    public String categoryToString(Category mode) {
        switch (mode) {
            case ENGLISH_DICTIONARY:
                return "English Dictionary";
            case DUTCH:
                return "Dutch";
            case ITALIAN:
                return "Italian";
            case NORWEGIAN:
                return "Norwegian";
            default: return "";
        }
    }
}

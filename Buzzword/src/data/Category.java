package data;

/**
 * Created by zhangxinyao on 11/11/16.
 * @author Xinyao Zhang
 */
public enum Category {
    ENGLISH_DICTIONARY(0),
    DUTCH(1),
    ITALIAN(2),
    NORWEGIAN(3);
    private int parameter;

    Category(int parameter) {
        this.parameter = parameter;
    }

    public int getParameter() {
        return parameter;
    }
}

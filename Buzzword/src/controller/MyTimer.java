package controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.EventHandler;
import javafx.scene.text.Text;
import javafx.util.Duration;
import state.EndLose;
import state.State;

import java.io.IOException;

/**
 * Created by zhangxinyao on 12/4/16.
 */
public class MyTimer {

    private PlayController play;
    private Timeline timeline;
    private Text timeText;
    private Integer remainingTime;

    public MyTimer(PlayController play){
        this.play=play;
        timeText = play.getSecond();
        remainingTime = play.getLimitTime();
    }

    public void countdown() {
        if (timeline != null)
            timeline.stop();
        timeText.setText(remainingTime.toString() + " s");
        timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        EventHandler keyframeEventHandler = event -> {
            if (remainingTime <= 0) {
                timeline.stop();
                play.setPauseState(new EndLose());
                State state = play.getPauseState();
                try {
                    state.controlPause(play);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else {
                timeText.setText((--remainingTime).toString()+" s");
            }
        };
        KeyFrame keyFrame = new KeyFrame(Duration.seconds(1), keyframeEventHandler);
        timeline.getKeyFrames().add(keyFrame);
        timeline.playFromStart();
    }
    public Timeline getTimeline(){ return timeline;}
}

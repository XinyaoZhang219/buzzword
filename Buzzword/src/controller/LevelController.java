package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.Workspace;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

/**
 * Created by zhangxinyao on 11/11/16.
 * @author Xinyao Zhang
 */
public class LevelController {

    private AppTemplate appTemplate;
    private Workspace workspace;
    private GameData gamedata;
    private BuzzwordController controller;

    private Button home = new Button("Home");

    private Text               modeText;
    private BorderPane         grid;
    private Text[]             level= new Text[8];
    private Circle[]           letterGridCircle;
    private StackPane[]        letterGridPane;
    private int                unlockLevel;


    public LevelController(AppTemplate app){
        appTemplate = app;
        gamedata = (GameData) app.getDataComponent();
    }

    public Text getModeText(){return modeText;}

    public void handleLevelSelection(){
        workspace = (Workspace) appTemplate.getWorkspaceComponent();
        controller = (BuzzwordController) appTemplate.getGUI().getFileController();
        controller.setCurrentScreen(Screen.LEVELSCREEN);
        updateButtom();
        VBox playPane = workspace.getPlayPane();
        modeText = controller.getHomeController().getModeText();
        playPane.getChildren().setAll(modeText);
        levelSelectionCircle();

        playPane.getChildren().add(grid);
        //clear play information
        VBox playInfoPane = workspace.getPlayInfoPane();
        playInfoPane.getChildren().setAll();
    }
    private void updateButtom(){
        VBox buttonPane = workspace.getButtonPane();
        Button profile = controller.getHomeController().getProfile();
        profile.setMaxWidth(Double.MAX_VALUE);
        home.setMaxWidth(Double.MAX_VALUE);
        buttonPane.getChildren().setAll(profile,home);
        profile.setOnAction(e -> {
            controller.setCurrentScreen(Screen.PROFILESCREEN);
            controller.getProfileController().handleProfileSetting();
        });
        home.setOnAction(e -> {
            controller.setCurrentScreen(Screen.HOMESCREEN);
            controller.getHomeController().loginedHomeScreen();
        });
    }

    private void levelSelectionCircle() {
        unlockLevel = gamedata.getPasslevel()[gamedata.getCurrentMode().getParameter()];
        grid = workspace.getGrid();
        letterGridPane = new StackPane[8];
        letterGridCircle = new Circle[8];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 4; j++) {
                int index = i * 4 + j;
                letterGridPane[index] = new StackPane();
                letterGridPane[index].prefWidthProperty().bind(grid.widthProperty().divide(4));
                letterGridPane[index].prefHeightProperty().bind(grid.heightProperty().divide(4));
                letterGridPane[index].layoutXProperty().bind(grid.widthProperty().divide(4)
                        .multiply(j).add(grid.widthProperty().divide(8)));
                letterGridPane[index].layoutYProperty().bind(grid.heightProperty().divide(4)
                        .multiply(i).add(grid.heightProperty().divide(8)));
                letterGridCircle[index] = new Circle(25);
                letterGridPane[index].getChildren().add(letterGridCircle[index]);
            }
        }
        grid.getChildren().setAll(letterGridPane);
        levelSelectionText();
    }

    private void levelSelectionText(){
        for (int i=0;i<8;i++){
            level[i] = new Text((i+1)+"");
            level[i].setFill(Paint.valueOf("white"));
            level[i].setMouseTransparent(true);
            letterGridCircle[i].setId("unlockcircle");
            letterGridPane[i].getChildren().add(level[i]);

        }
        unlockLevel = gamedata.getPasslevel()[gamedata.getCurrentMode().getParameter()];

        for (int i=0;i<unlockLevel+1;i++){
            if (i < 8){
                letterGridCircle[i].setId("lockcircle");
                level[i].setFill(Paint.valueOf("Black"));
            }
        }
        levelSeletionAction();
    }


    private void levelSeletionAction() {
       for (int i=0;i<8;i++){
           setLevelAction(letterGridCircle[i],unlockLevel,i);
       }
    }

    private void setLevelAction(Circle c, int unlockLevel, int index) {
        if (unlockLevel >= index){
            c.setOnMouseClicked(e ->{
                gamedata.setPlayLevel(index+1);
                    controller.setCurrentScreen(Screen.PLAYSCREEN);
                    controller.getPlayController().handlePlay();
            });

        }
    }



}

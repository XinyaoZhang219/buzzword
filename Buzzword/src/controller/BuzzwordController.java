package controller;

import apptemplate.AppTemplate;
import data.GameData;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import propertymanager.PropertyManager;
import state.ModePaneExpand;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * Created by zhangxinyao on 11/9/16.
 * @author Xinyao Zhang
 */
public class BuzzwordController implements FileController {

    PropertyManager propertyManager = PropertyManager.getManager();
    private AppTemplate appTemplate;
    private GameData gamedata;
    private HomeController homeController;
    private LevelController levelController;
    private PlayController playController;
    private ProfileController profileController;
    private HelpController helpController;
    private Screen currentScreen;

    public BuzzwordController(AppTemplate app){
        appTemplate = app;
        homeController = new HomeController(appTemplate);
        levelController = new LevelController(appTemplate);
        playController = new PlayController(appTemplate);
        profileController = new ProfileController(appTemplate);
        helpController = new HelpController(appTemplate);
        currentScreen = Screen.HOMESCREEN;


    }

    public HomeController getHomeController(){  return  homeController;}
    public LevelController getLevelController(){ return levelController;}
    public PlayController getPlayController(){ return  playController; }
    public ProfileController getProfileController(){ return  profileController; }

    @Override
    public void handleNewRequest() {

    }

    @Override
    public void handleSaveRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        gamedata = (GameData) appTemplate.getDataComponent();
        Path        appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
        Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
        Path        profilePath = Paths.get(targetPath.toString()+File.separator+gamedata.getName());
        appTemplate.getFileComponent().saveData(gamedata,profilePath);
    }

    @Override
    public void handleLoadRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        gamedata = (GameData) appTemplate.getDataComponent();
        Path        appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
        Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
        Path        profilePath = Paths.get(targetPath.toString()+ File.separator+gamedata.getName());
        appTemplate.getFileComponent().loadData(gamedata,profilePath);
    }

    @Override
    public void handleExitRequest() {
        if (playController.getPlaying()) {
            playController.pauseStateControl();
        }
        YesNoCancelDialogSingleton dialog = YesNoCancelDialogSingleton.getSingleton();
        PropertyManager props = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(EXIT_TITLE), props.getPropertyValue(EXIT_MESSAGE));
        if (dialog.getSelection().equals(YesNoCancelDialogSingleton.YES)) {
            try {
                handleSaveRequest();
            } catch (IOException e) {
                AppMessageDialogSingleton dialogSingleton = AppMessageDialogSingleton.getSingleton();
                dialogSingleton.show(propertyManager.getPropertyValue(SAVE_ERROR_TITLE),
                        propertyManager.getPropertyValue(SAVE_ERROR_MESSAGE));
            } catch (NullPointerException e) {
                //ignore
            }
            System.exit(0);
        }
    }

    public void handleHelpRequest(){
        if (currentScreen == Screen.PLAYSCREEN) {
            if (playController.getPlaying()) {
                playController.pauseStateControl();
            }
            YesNoCancelDialogSingleton dialog = YesNoCancelDialogSingleton.getSingleton();
            PropertyManager props = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(LEAVE_TITLE), props.getPropertyValue(LEAVE_MESSAGE));
            if (dialog.getSelection().equals(YesNoCancelDialogSingleton.YES)) {
                helpController.handleHelp();
            }
        }else {
            helpController.handleHelp();
        }
    }

    public void setCurrentScreen(Screen screen){
        currentScreen = screen;
    }
    public Screen getCurrentScreen(){ return currentScreen;}

    public void shortcut(){
        gamedata = (GameData) appTemplate.getDataComponent();
        Scene scene = appTemplate.getGUI().getPrimaryScene();
        scene.setOnKeyReleased(e -> {
            if (getCurrentScreen() == Screen.HOMESCREEN && !gamedata.logined) {
                if (e.getCode() == KeyCode.P && e.isControlDown() && e.isShiftDown()) {
                    homeController.handleCreateNewProfile();
                } else if (e.getCode() == KeyCode.L && e.isControlDown()) {
                    homeController.handleLogin();
                }
            } else if (getCurrentScreen() == Screen.HOMESCREEN && gamedata.logined) {
                if (e.getCode() == KeyCode.P && e.isControlDown()) {
                    homeController.setModePaneState(new ModePaneExpand());
                    levelController.handleLevelSelection();
                } else if (e.getCode() == KeyCode.L && e.isControlDown()) {
                    homeController.logoutAction();
                }
            } else if (getCurrentScreen() == Screen.PLAYSCREEN) {
                if (e.getCode() == KeyCode.R && e.isControlDown()) {
                    playController.setPlayRestartAction();
                } else if (e.getCode() == KeyCode.H && e.isControlDown()) {
                    playController.setPlayHomeAction();
                }
            } else if (getCurrentScreen() != Screen.HOMESCREEN && e.getCode() == KeyCode.H && e.isControlDown()) {
                homeController.loginedHomeScreen();
            }
            if (e.getCode() == KeyCode.Q && e.isControlDown()) {
                handleExitRequest();
            }
            if (e.getCode() == KeyCode.S && e.isControlDown()){
                if (!appTemplate.getGUI().getSaveButton().isDisabled()){
                    appTemplate.getGUI().setSaveDisable(true);
                    try {
                        handleSaveRequest();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
    }
}

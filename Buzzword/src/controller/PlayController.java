package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.Workspace;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import propertymanager.PropertyManager;
import state.EndLose;
import state.EndWin;
import state.Play;
import state.State;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_IMAGEDIR_PATH;

/**
 * Created by zhangxinyao on 11/11/16.
 * @author Xinyao Zhang
 */
public class PlayController {
    PropertyManager propertyManager = PropertyManager.getManager();

    private AppTemplate appTemplate;

    private Workspace workspace;
    private GameData gamedata;

    private BuzzwordController controller;

    private Button home = new Button("Home");
    private Button restart = new Button("Restart");

    private VBox               playPane;
    private BorderPane         grid;
    private Line[][]           Hlines;
    private Line[][]           Vlines;
    private Circle[][]         letterGridCircle;
    private Text[][]           word;
    private Button             pause;
    private State              pauseState;
    private boolean            playing;

    private VBox            playInfoPane;
    private Text            second;
    private  MyTimer        timer;
    private TextField       input;
    private VBox            pointPane;
    private ScrollPane      pointRecordPane = new ScrollPane();
    private GridPane pointRecord = new GridPane();
    private HBox            totalPane = new HBox(10);
    private Label           totalScoreLabel = new Label();
    private VBox            targetPointPane = new VBox(5);

    private int limitTime;
    private String guess;
    private int lastDraggedRow;
    private int lastDraggedCol;
    private int index;

    private HashSet<String> guessedWords;
    private HashSet<boolean[][]> pathNode;
    private HashSet<int[][]> path;

    public PlayController(AppTemplate app){
        appTemplate = app;
        gamedata = (GameData) app.getDataComponent();
    }
    public void handlePlay() {
        workspace = (Workspace) appTemplate.getWorkspaceComponent();
        controller = (BuzzwordController) appTemplate.getGUI().getFileController();
        controller.setCurrentScreen(Screen.PLAYSCREEN);
        playing = false;
        gamedata.setupTargetWords();
        limitTime = gamedata.getLimitTime();
        pauseState = new Play();
        updateButtomPane();
        playPane = workspace.getPlayPane();
        updatePlayPane();
        playInfoPane = workspace.getPlayInfoPane();
        index=0;
        updatePlayInfoPane();
        input.setDisable(true);
        play();
    }

    private void updateButtomPane() {

        VBox buttonPane = workspace.getButtonPane();
        Button profile = controller.getHomeController().getProfile();
        profile.setMaxWidth(Double.MAX_VALUE);
        home.setMaxWidth(Double.MAX_VALUE);
        restart.setMaxWidth(Double.MAX_VALUE);
        buttonPane.getChildren().setAll(profile, home, restart);
        profile.setOnAction(e -> setPlayProfileAction());
        home.setOnAction(e -> setPlayHomeAction());
        restart.setOnAction(e->setPlayRestartAction());

    }
    public void setPlayProfileAction() {
        if (playing) {
            pauseStateControl();
        }
        YesNoCancelDialogSingleton dialog = YesNoCancelDialogSingleton.getSingleton();
        PropertyManager props = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(LEAVE_TITLE), props.getPropertyValue(LEAVE_MESSAGE));
        if (dialog.getSelection().equals(YesNoCancelDialogSingleton.YES)) {
            playing = false;
            grid.setMouseTransparent(false);
            controller.setCurrentScreen(Screen.PROFILESCREEN);
            controller.getProfileController().handleProfileSetting();
        }
    }
    public void setPlayHomeAction() {
        if (playing) {
            pauseStateControl();
        }
        YesNoCancelDialogSingleton dialog = YesNoCancelDialogSingleton.getSingleton();
        PropertyManager props = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(LEAVE_TITLE), props.getPropertyValue(LEAVE_MESSAGE));
        if (dialog.getSelection().equals(YesNoCancelDialogSingleton.YES)) {
            playing = false;
            grid.setMouseTransparent(false);
            controller.setCurrentScreen(Screen.HOMESCREEN);
            controller.getHomeController().loginedHomeScreen();
        }
    }
    public void setPlayRestartAction() {
            if (timer.getTimeline() != null) {
                timer.getTimeline().stop();
            }
            grid.setMouseTransparent(false);
            handlePlay();
    }



    private void updatePlayPane() {
        Text modeText = controller.getLevelController().getModeText();
        grid = workspace.getGrid();
        grid.getChildren().clear();
        initLines();
        workspace.initLetterGirdPane();
        letterGridCircle = workspace.getLetterGridCircle();
        word = workspace.getWord();

        Text level = new Text("Level " + gamedata.getPlayLevel());
        level.setId("levelText");
        pause = new Button();
        try {
            setPauseButton(PLAY_ICON.toString(),PLAY_TOOLTIP.toString());
        } catch (IOException e) {
            AppMessageDialogSingleton d = AppMessageDialogSingleton.getSingleton();
            d.show("Image Error",e.getMessage());
        }
        pause.setOnAction( e -> pauseStateControl());
        playPane.getChildren().setAll(modeText,grid, level,pause);
    }
    private void initLines(){
        Hlines = new Line[4][3];
        Vlines = new Line[3][4];
        for (int i =0; i<4;i++){
            for (int j=0;j<3;j++){
                Hlines[i][j] = new Line();
                Vlines[j][i] = new Line();
                grid.getChildren().addAll(Hlines[i][j],Vlines[j][i]);
            }
        }

        for (int i=0;i<4;i++){
            for (int j=0;j<4;j++){
                if (j<3){
                    Hlines[i][j].startXProperty().bind(grid.widthProperty().divide(4)
                            .multiply(j).add(grid.widthProperty().divide(8)));
                    Hlines[i][j].startYProperty().bind(grid.heightProperty().divide(4)
                            .multiply(i).add(grid.heightProperty().divide(8)));
                    Hlines[i][j].endXProperty().bind(grid.widthProperty().divide(4)
                            .multiply(j+1).add(grid.widthProperty().divide(8)));
                    Hlines[i][j].endYProperty().bind(grid.heightProperty().divide(4)
                            .multiply(i).add(grid.heightProperty().divide(8)));
                    Hlines[i][j].setStrokeWidth(3);
                    Hlines[i][j].setStroke(Paint.valueOf("#516063"));
                }
            }
        }
        for (int i=0;i<4;i++) {
            for (int j = 0; j < 4; j++) {
                if (i<3){
                    Vlines[i][j].startXProperty().bind(grid.widthProperty().divide(4)
                            .multiply(j).add(grid.widthProperty().divide(8)));
                    Vlines[i][j].startYProperty().bind(grid.heightProperty().divide(4)
                            .multiply(i).add(grid.heightProperty().divide(8)));
                    Vlines[i][j].endXProperty().bind(grid.widthProperty().divide(4)
                            .multiply(j).add(grid.widthProperty().divide(8)));
                    Vlines[i][j].endYProperty().bind(grid.heightProperty().divide(4)
                            .multiply(i+1).add(grid.heightProperty().divide(8)));
                    Vlines[i][j].setStrokeWidth(3);
                    Vlines[i][j].setStroke(Paint.valueOf("#516063"));

                }
            }
        }
    }



    private void updatePlayInfoPane() {
        HBox secondPane = new HBox(10);
        secondPane.setMaxWidth(Double.MAX_VALUE);
        Text timeText = new Text("TIME REMAINING: ");
        timeText.setUnderline(true);
        timeText.setFill(Paint.valueOf("red"));
        second = new Text(limitTime+" s");
        timer = new MyTimer(this);
        second.setFill(Paint.valueOf("red"));
        secondPane.getChildren().setAll(timeText,second);


        input = new TextField();
        input.setPromptText("Enter your guess");
        pointPane = new VBox();
        initPointPane();

        targetPointPane.setPadding(new Insets(5,5,5,5));
        targetPointPane.setStyle("-fx-border-color: black;-fx-border-radius:5");
        Text targetText = new Text("TARGET");
        targetText.setUnderline(true);
        Text targetPoint = new Text(gamedata.getTargetPoint()+" points");
        Text totalText = new Text("MAX SCROE");
        totalText.setUnderline(true);
        Text totalPoint = new Text(gamedata.getTotalPoint()+" points");
        targetPointPane.getChildren().setAll(targetText,targetPoint,totalText,totalPoint);

        playInfoPane.getChildren().setAll(secondPane,input,pointPane,targetPointPane);
    }

    private void initPointPane() {

        pointPane.setPrefSize(160,200);
        //scrollPane
        pointRecordPane.setPrefSize(160,180);
        pointRecordPane.setId("scrollPane");
        //container of word and score
        pointRecord.setStyle("-fx-background-color: transparent");
        pointRecord.setHgap(80);
        pointRecord.getChildren().clear();
        pointRecordPane.setContent(pointRecord);

        Label tot = new Label("TOTAL");
        tot.setPrefWidth(110);
        totalScoreLabel.setText(gamedata.getTotalscore()+"");
        totalPane.getChildren().setAll(tot,totalScoreLabel);

        pointPane.getChildren().setAll(pointRecordPane,totalPane);
    }

    private void play() {
        guessedWords = gamedata.getGuessedWord();
        mouseDrag();
        type();

    }
    private void type(){
        pathNode = new HashSet<>();
        path = new HashSet<>();
        input.setOnKeyReleased(e ->{
            guess="";
            pathNode.clear();
            path.clear();
            if (input.getText().length()!=0) {
                if (e.getCode()== KeyCode.ENTER) {
                    if (gamedata.getAllTargetWords().contains(input.getText()) && !guessedWords.contains(input.getText())){
                        guseeRightAndcheckEnd(input.getText());
                    }
                    clearHighlight();
                    input.clear();
                }else {
                    int length = input.getText().length();
                    findTypePath(length, input.getText().toLowerCase());
                    clearHighlight();
                    highlight(length);
                }
            }else
                clearHighlight();
        });
    }
    public void highlight(int length){
        for (boolean[][] p: pathNode) {
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    if (p[i][j])
                        letterGridCircle[i][j].setFill(Paint.valueOf("#12b2d2"));
                }
            }
        }
        int lastRow;
        int lastCol;
        for(int[][] p :path){
            lastRow=-1;
            lastCol=-1;
            for (int i = 0; i <length ; i++) {

                if (lastRow ==-1 && lastCol == -1){
                    lastRow = p[0][i];
                    lastCol = p[1][i];
                }else if (p[0][i] == lastRow+1 && p[1][i] == lastCol){
                    Vlines[lastRow][lastCol].setStroke(Paint.valueOf("#12b2d2"));
                    lastRow += 1;
                }else if (p[0][i] == lastRow-1 && p[1][i] == lastCol){
                    Vlines[lastRow-1][lastCol].setStroke(Paint.valueOf("#12b2d2"));
                    lastRow -= 1;
                }else if (p[0][i] == lastRow && p[1][i] == lastCol+1){
                    Hlines[lastRow][lastCol].setStroke(Paint.valueOf("#12b2d2"));
                    lastCol +=1;
                }else if(p[0][i] == lastRow && p[1][i] == lastCol-1){
                    Hlines[lastRow][lastCol-1].setStroke(Paint.valueOf("#12b2d2"));
                    lastCol -=1;
                }

            }

        }

    }
    private void findTypePath(int length,String target){

        boolean[][] used ;
        int [][] coordinate;
        for (int i = 0; i <4 ; i++) {
            for (int j = 0; j <4 ; j++) {
                if (word[i][j].getText().equals(target.charAt(0)+"")) {
                    used= new boolean[4][4];
                    coordinate = new int[2][length];
                    for (int a = 0; a < 4; a++) {
                        for (int b = 0; b < 4; b++) {
                            used[a][b] = false;
                        }
                    }
                    for (int a = 0; a < 2; a++) {
                        for (int b = 0; b < length; b++) {
                            coordinate[a][b] = -1;
                        }
                    }
                    findTypePathHelper(length, target ,i, j,used,coordinate);

                }
            }

        }

    }
    private void findTypePathHelper(int length, String target, int row, int col, boolean[][] used, int[][] coordinate){
        used[row][col] = true;
        for (int a = 0; a < target.length(); a++) {
            if (coordinate[0][a]==-1){
                coordinate[0][a]=row;
                coordinate[1][a]=col;
                break;
            }
        }
        if (length<=1) {
            boolean[][] clone1 = new boolean[4][4];
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    clone1[i][j] = used[i][j];
                }
            }
            int[][] clone2 = new int[2][target.length()];
            for (int a = 0; a < target.length(); a++) {
                clone2[0][a] = coordinate[0][a];
                clone2[1][a] = coordinate[1][a];
            }
            pathNode.add(clone1);
            path.add(clone2);
//            coordinate[0][target.length()-1]=-1;
//            coordinate[1][target.length()-1]=-1;
            return;

        }
        if (row-1>=0 && !used[row-1][col] && word[row-1][col].getText().equals(target.charAt(target.length()-length+1)+"")){
            findTypePathHelper(length-1,target,row-1,col,used,coordinate);
            used[row-1][col] = false;
            if(coordinate[0][target.length()-1] != -1) {
                coordinate[0][target.length() - 1] = -1;
                coordinate[1][target.length() - 1] = -1;
            }else {
                for (int a = 1; a < target.length(); a++) {
                    if (coordinate[0][a] == -1) {
                        coordinate[0][a - 1] = -1;
                        coordinate[1][a - 1] = -1;
                        break;
                    }
                }
            }
        }
        if (col+1<4 && !used[row][col+1] && word[row][col+1].getText().equals(target.charAt(target.length()-length+1)+"")){
            findTypePathHelper(length-1,target,row,col+1,used,coordinate);
            used[row][col+1] = false;
            if(coordinate[0][target.length()-1] != -1) {
                coordinate[0][target.length() - 1] = -1;
                coordinate[1][target.length() - 1] = -1;
            }else {
                for (int a = 1; a < target.length(); a++) {
                    if (coordinate[0][a] == -1) {
                        coordinate[0][a - 1] = -1;
                        coordinate[1][a - 1] = -1;
                        break;
                    }
                }
            }

        }
        if (row+1<4 && !used[row+1][col] &&word[row+1][col].getText().equals(target.charAt(target.length()-length+1)+"")){
            findTypePathHelper(length-1,target,row+1,col,used,coordinate);
            used[row+1][col] = false;
            if(coordinate[0][target.length()-1] != -1) {
                coordinate[0][target.length() - 1] = -1;
                coordinate[1][target.length() - 1] = -1;
            }else {
                for (int a = 1; a < target.length(); a++) {
                    if (coordinate[0][a] == -1) {
                        coordinate[0][a - 1] = -1;
                        coordinate[1][a - 1] = -1;
                        break;
                    }
                }
            }

        }
        if (col-1>=0 && !used[row][col-1] &&word[row][col-1].getText().equals(target.charAt(target.length()-length+1)+"")){
            findTypePathHelper(length-1,target,row,col-1,used,coordinate);
            used[row][col-1] = false;
            if(coordinate[0][target.length()-1] != -1) {
                coordinate[0][target.length() - 1] = -1;
                coordinate[1][target.length() - 1] = -1;
            }else {
                for (int a = 1; a < target.length(); a++) {
                    if (coordinate[0][a] == -1) {
                        coordinate[0][a - 1] = -1;
                        coordinate[1][a - 1] = -1;
                        break;
                    }
                }
            }
         }


    }
    public void clearHighlight(){
        for (int i = 0; i <4 ; i++) {
            for (int j = 0; j <4 ; j++) {
                letterGridCircle[i][j].setFill(Paint.valueOf("#516063"));
                if (j<3){
                    Hlines[i][j].setStroke(Paint.valueOf("#516063"));
                    Vlines[j][i].setStroke(Paint.valueOf("#516063"));
                }
            }
        }
    }
    private void mouseDrag(){
        for (int i = 0; i <4 ; i++) {
            for (int j = 0; j <4 ; j++) {
                setPress(letterGridCircle[i][j]);
            }
        }
        boolean[][] dragged = new boolean[4][4];
        grid.setOnDragDetected( e -> {
            clearHighlight();
            grid.startFullDrag();
            guess = "";
            lastDraggedRow = -1;
            lastDraggedCol = -1;
            for (int i = 0; i <4 ; i++) {
                for (int j = 0; j <4 ; j++) {
                    dragged[i][j] = false;
                    letterGridCircle[i][j].setMouseTransparent(false);
                }
            }
            for (int i = 0; i <4 ; i++) {
                for (int j = 0; j <4 ; j++) {
                   setDragEnter(letterGridCircle[i][j],i,j,dragged);
                }
            }
        });
        workspace.getWorkspace().setOnMouseDragReleased(e ->{
            clearHighlight();
            if (gamedata.getAllTargetWords().contains(guess) && !guessedWords.contains(guess)) {
                guseeRightAndcheckEnd(guess);
            }
            highlight(input.getText().length());
        });

    }
    private void setPress(Circle c){
        c.setOnMousePressed(e ->c.setFill(Paint.valueOf("#12b2d2")));
        c.setOnMouseClicked(e -> {
            c.setFill(Paint.valueOf("#516063"));
            clearHighlight();
            highlight(input.getText().length());
        });
    }
    private void setDragEnter(Circle c,int row,int col,boolean[][] dragged){
        c.setOnMouseDragEntered(e -> {
            if (!dragged[row][col] && !word[row][col].getText().isEmpty()
                    && (lastDraggedRow ==-1 && lastDraggedCol==-1) ){
                setDragEnterHelper(c,row,col,dragged);
            }else if(!dragged[row][col] && !word[row][col].getText().isEmpty()) {

                if (row == lastDraggedRow + 1 && col == lastDraggedCol) {
                    Vlines[row-1][col].setStroke(Paint.valueOf("#12b2d2"));
                    setDragEnterHelper(c,row,col,dragged);

                }else if (row == lastDraggedRow - 1 && col == lastDraggedCol) {
                    Vlines[row][col].setStroke(Paint.valueOf("#12b2d2"));
                    setDragEnterHelper(c,row,col,dragged);

                }else if (row == lastDraggedRow && col == lastDraggedCol + 1) {
                    Hlines[row][col-1].setStroke(Paint.valueOf("#12b2d2"));
                    setDragEnterHelper(c,row,col,dragged);

                }else if (row == lastDraggedRow && col == lastDraggedCol - 1){
                    Hlines[row][col].setStroke(Paint.valueOf("#12b2d2"));
                    setDragEnterHelper(c,row,col,dragged);

                }


            }else{
                mouseDragRepeat();
            }
        });
    }
    private void setDragEnterHelper(Circle c,int row,int col,boolean[][] dragged){
        c.setFill(Paint.valueOf("#12b2d2"));
        dragged[row][col]=true;
        guess+=word[row][col].getText();
        lastDraggedRow = row;
        lastDraggedCol = col;
    }
    private void mouseDragRepeat(){
        guess="";
        for (int i=0;i<4;i++){
            for (int j=0;j<4;j++){
                letterGridCircle[i][j].setFill(Paint.valueOf("#516063"));
                letterGridCircle[i][j].setMouseTransparent(true);
                if (j<3){
                    Hlines[i][j].setStroke(Paint.valueOf("#516063"));
                    Vlines[j][i].setStroke(Paint.valueOf("#516063"));
                }
            }
        }
    }

    private void guseeRightAndcheckEnd(String guess){
        guessedWords.add(guess);
        pointRecord.add(new Text(guess),0,index);
        pointRecord.add(new Text(guess.length()+""),1,index++);
        gamedata.setTotalScore(gamedata.getTotalscore()+guess.length());
        totalScoreLabel.setText(gamedata.getTotalscore()+"");
        if (gamedata.getTotalscore()>=gamedata.getTargetPoint()) {
            if (gamedata.getPlayLevel() <= 8) {
                if (gamedata.getPlayLevel() == 8) {
                    setPauseState(new EndLose());
                    pauseStateControl();
                }else {
                    setPauseState(new EndWin());
                    pauseStateControl();
                }
                if (gamedata.getPlayLevel() > gamedata.getPasslevel()[gamedata.getCurrentMode().getParameter()]) {
                    gamedata.setPasslevel(gamedata.getCurrentMode(), gamedata.getPlayLevel());
                    appTemplate.getGUI().setSaveDisable(false);
                }
            }
        }

    }
    public void showAnswer(){
        for (String s:gamedata.getAllTargetWords()) {
            if (!gamedata.getGuessedWord().contains(s)){

                gamedata.getGuessedWord().add(s);
                Text w = new Text(s);
                w.setFill(Paint.valueOf("red"));
                w.setFont(Font.font(12));
                pointRecord.add(w,0,index);
                Text n = new Text(s.length()+"");
                n.setFill(Paint.valueOf("red"));
                n.setFont(Font.font(12));
                pointRecord.add(n,1,index++);

            }
        }

    }

    public void setPauseButton(String icon, String tooltip) throws IOException {

        URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
        if (imgDirURL == null)
            throw new FileNotFoundException("Image resources folder does not exist.");

        try (InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(propertyManager.getPropertyValue(icon)))) {
            Image buttonImage = new Image(imgInputStream);
            ImageView buttonImageView = new ImageView(buttonImage);
            pause.setGraphic(buttonImageView);
            Tooltip buttonTooltip = new Tooltip(propertyManager.getPropertyValue(tooltip));
            pause.setTooltip(buttonTooltip);
        } catch (URISyntaxException e) {
            AppMessageDialogSingleton d = AppMessageDialogSingleton.getSingleton();
            d.show("Image Error","Can not find such image or file");
            System.exit(1);
        }

    }
    public void pauseStateControl(){
        try {
            pauseState.controlPause(this);
        } catch (IOException e) {
            AppMessageDialogSingleton d = AppMessageDialogSingleton.getSingleton();
            d.show("Image Error",e.getMessage());

        }
    }

    public void setPlaying(boolean b){ playing = b;}
    public void setPauseState(State state){ pauseState = state; }
    public Text getSecond(){ return second;}
    public Text[][] getWord(){ return  word;}
    public GameData getGamedata(){ return gamedata;}
    public boolean getPlaying(){ return playing;}
    public MyTimer getTimer(){ return timer;}
    public int getLimitTime(){ return limitTime;}
    public TextField getInput(){ return input;}
    public State getPauseState(){ return pauseState;}
    public BorderPane getGrid(){ return grid;}
    public void setGuess(String s){ guess = s;}
    public Button getPause(){ return pause;}

}

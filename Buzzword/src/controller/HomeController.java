package controller;

import apptemplate.AppTemplate;
import data.Category;
import data.GameData;
import gui.LoginDialog;
import gui.Workspace;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import propertymanager.PropertyManager;
import state.ModePaneExpand;
import state.State;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * Created by zhangxinyao on 11/11/16.
 * @author Xinyao Zhang
 */
public class HomeController {


    private AppTemplate appTemplate;
    private Workspace workspace;
    private GameData gamedata;
    private BuzzwordController controller;
    private PropertyManager propertyManager = PropertyManager.getManager();

    private State modePaneState = new ModePaneExpand();

    private Button             profile;
    private VBox               modePane;
    private Button             select;
    private Label[]            mode;
    private Button             start = new Button("Start Playing");
    private Button             logout = new Button("Logout");

    private Text               modeText;
    private BorderPane         grid;

    private VBox               playInfoPane;


    public HomeController(AppTemplate app){
        appTemplate = app;
        gamedata = (GameData) app.getDataComponent();


    }

    public void handleCreateNewProfile(){
        LoginDialog create = LoginDialog.getSingleton();
        create.setError("",false);
        create.getAccount().clear();
        create.getPassword().clear();
        create.getScene().setOnKeyPressed(e ->{
            if (e.getCode() == KeyCode.ESCAPE)
                create.close();
            if (e.getCode() == KeyCode.ENTER){
                String userAccount = create.getAccount().getText();
                String password = create.getPassword().getText();
                PropertyManager propertyManager = PropertyManager.getManager();
                Path appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
                Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
                Path        profilePath = Paths.get(targetPath.toString()+"/"+userAccount);
                File        file        = profilePath.toFile();
                if (userAccount.equals("")|| password.equals("")){
                    create.setError("Account or password can not be empty",true);
                } else if (file.exists()){
                    create.setError("Account exist",true);
                }else {
                    create.close();
                    try {
                        setupNewUser(userAccount, password);            //setup GameData
                    } catch (IOException e1) {
                        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                        dialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE),
                                propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
                    }
                    loginedHomeScreen();
                }
            }
        });
        create.show("Create New Profile");

    }

    public void handleLogin(){
        LoginDialog log = LoginDialog.getSingleton();
        log.getAccount().clear();
        log.getPassword().clear();
        log.getScene().setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ESCAPE)
                log.close();
            if (e.getCode() == KeyCode.ENTER) {
                String userAccount = log.getAccount().getText();
                String password = log.getPassword().getText();
                try {
                    if (!userAccount.equals("")) {
                        if (checkUser(userAccount, password)) {
                            log.close();
                            loginedHomeScreen();
                        }
                    }
                } catch (IOException e1) {
                    PropertyManager propertyManager = PropertyManager.getManager();
                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    dialog.show(propertyManager.getPropertyValue(LOAD_ERROR_TITLE),
                            propertyManager.getPropertyValue(LOAD_ERROR_MESSAGE));
                }
            }
        });
        log.show("Login");

    }


    private void setupNewUser(String name, String password) throws IOException {
        gamedata.reset();
        gamedata.setName(name);
        gamedata.setPassword(password);
        appTemplate.getGUI().getFileController().handleSaveRequest();
        gamedata.logined = true;
    }


    private boolean checkUser(String name, String password) throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        Path appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
        Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
        Path        profilePath = Paths.get(targetPath.toString()+"/"+name);
        File        file        = profilePath.toFile();
        if (!file.exists()) {
            LoginDialog log = LoginDialog.getSingleton();
            log.setError("Accout dose not exist",true);
            return false;
        }else{
            gamedata.reset();
            gamedata.setName(name);
            appTemplate.getGUI().getFileController().handleLoadRequest();
            if (!encode(password).equals(gamedata.getPassword())) {
                LoginDialog log = LoginDialog.getSingleton();
                log.setError("Uncorrent password", true);
                return false;
            }else {
                gamedata.setPassword(password);
                gamedata.logined = true;
            }
        }return true;

    }




    public void loginedHomeScreen(){
        workspace = (Workspace) appTemplate.getWorkspaceComponent();
        VBox buttonPane = workspace.getButtonPane();
        controller = (BuzzwordController) appTemplate.getGUI().getFileController();
        controller.setCurrentScreen(Screen.HOMESCREEN);
        //update button in left side
        profile = new Button(gamedata.getName());
        profile.setMaxWidth(Double.MAX_VALUE);

        //select mode
        modePane = new VBox();
        modePane.setMaxWidth(Double.MAX_VALUE);
        modePane.setAlignment(Pos.CENTER);
        select = new Button("Select Mode");
        select.setPrefWidth(160);
        modePane.getChildren().add(select);
        initModeAndAction();

        start.setMaxWidth(Double.MAX_VALUE);
        profile.setOnAction(e -> {
            controller.getProfileController().handleProfileSetting();
        });
        select.setOnAction(e ->modePaneControl());
        start.setOnAction(e -> {
            this.setModePaneState(new ModePaneExpand());
            controller.getLevelController().handleLevelSelection();
        });
        logout.setMaxWidth(Double.MAX_VALUE);
        logout.setOnAction(e -> logoutAction());
        buttonPane.getChildren().setAll(profile,modePane,start,logout);


        // clear playPane
        VBox playPane = workspace.getPlayPane();
        grid = workspace.getGrid();
        grid.getChildren().setAll();
        modeText = new Text(gamedata.categoryToString(gamedata.getCurrentMode()));
        modeText.setId("modeText");

        workspace.initLetterGirdPane();
        workspace.buzzwordText();

        playPane.getChildren().setAll(modeText,workspace.getGrid());
        //clear play information
        playInfoPane = workspace.getPlayInfoPane();
        playInfoPane.getChildren().setAll();

    }
    public void logoutAction() {
        YesNoCancelDialogSingleton dialogSingleton = YesNoCancelDialogSingleton.getSingleton();
        dialogSingleton.show(propertyManager.getPropertyValue(LOGOUT_TITLE.toString()),
                propertyManager.getPropertyValue(LOGOUT_MESSAGE.toString()));
        if (dialogSingleton.getSelection().equals(YesNoCancelDialogSingleton.YES)) {
            gamedata.logined = false;
            try {
                controller.handleSaveRequest();
            } catch (IOException e1) {
                AppMessageDialogSingleton d = AppMessageDialogSingleton.getSingleton();
                d.show(propertyManager.getPropertyValue(SAVE_ERROR_TITLE),
                        propertyManager.getPropertyValue(SAVE_ERROR_MESSAGE));
            }
            workspace.initHomeScreen();
        }
    }

    public void modePaneControl(){
        modePaneState.controlPane(this);
    }
    public void setModePaneState(State state){
        modePaneState = state;
    }

    private void initModeAndAction(){
        mode = new Label[4];
        for (int i=0;i<4;i++){
            mode[i] = new Label();
            mode[i].setId("modeSelection");
            mode[i].setMaxWidth(Double.MAX_VALUE);
            setModeAction(i);

        }
        mode[0].setText(gamedata.categoryToString(Category.ENGLISH_DICTIONARY));
        mode[1].setText(gamedata.categoryToString(Category.DUTCH));
        mode[2].setText(gamedata.categoryToString(Category.ITALIAN));

        mode[3].setText(gamedata.categoryToString(Category.NORWEGIAN));



    }

    private void setModeAction(int i){
        mode[i].setOnMouseClicked(e->{
            switch (i){
                case 0: gamedata.setCurrentMode(Category.ENGLISH_DICTIONARY);break;
                case 1: gamedata.setCurrentMode(Category.DUTCH);break;
                case 2: gamedata.setCurrentMode(Category.ITALIAN);break;
                case 3: gamedata.setCurrentMode(Category.NORWEGIAN);break;
            }
            modeText.setText(gamedata.categoryToString(gamedata.getCurrentMode()));
            modePaneControl();
        });

    }

    public VBox getModePane(){  return modePane; }
    public Label[] getMode(){   return mode;}
    public Button getSelect(){  return select;}
    public Text getModeText(){ return modeText;}
    public Button   getProfile(){ return profile;}

    private String encode(String password) {
        StringBuffer hexValue = new StringBuffer();
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            char[] charArray = password.toCharArray();
            byte[] byteArray = new byte[charArray.length];

            for (int i = 0; i < charArray.length; i++)
                byteArray[i] = (byte) charArray[i];
            byte[] md5Bytes = md5.digest(byteArray);
            for (int i = 0; i < md5Bytes.length; i++) {
                int val = ((int) md5Bytes[i]) & 0xff;
                if (val < 16)
                    hexValue.append("0");
                hexValue.append(Integer.toHexString(val));
            }
        }catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        }
        return hexValue.toString();

    }
}

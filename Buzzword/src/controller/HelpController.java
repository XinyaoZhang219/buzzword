package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.Workspace;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * Created by zhangxinyao on 12/10/16.
 */
public class HelpController {
    private AppTemplate appTemplate;
    private Workspace workspace;
    private GameData gamedata;

    private BuzzwordController controller;
    private Button home= new Button("Home");

    private ScrollPane help = new ScrollPane();

    public HelpController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        setHelpContent();
    }
    public void handleHelp(){
        workspace = (Workspace) appTemplate.getWorkspaceComponent();
        controller = (BuzzwordController) appTemplate.getGUI().getFileController();
        gamedata = (GameData) appTemplate.getDataComponent();
        VBox buttonPane = workspace.getButtonPane();

        home.setOnAction(e -> {
            if (gamedata.logined)
                controller.getHomeController().loginedHomeScreen();
            else
                workspace.initHomeScreen();
        });
        home.setMaxWidth(Double.MAX_VALUE);
        buttonPane.getChildren().setAll(home);
        VBox playPane = workspace.getPlayPane();

        VBox playInfoPlan = workspace.getPlayInfoPane();
        playInfoPlan.getChildren().setAll();
//        help.setPrefSize(400,400);
        help.setId("help");
        playPane.getChildren().setAll(help);
        help.setOnKeyPressed(e ->{
            if (e.getCode() == KeyCode.UP){
                help.setVvalue(help.getVvalue()-0.2);
            }else if (e.getCode() == KeyCode.DOWN){
                help.setVvalue(help.getVvalue()+0.2);
            }
        });

    }

    private void setHelpContent(){
        GridPane information = new GridPane();
        information.setHgap(20);
        information.setVgap(20);
        information.setMouseTransparent(true);


        Text login = new Text("Create a new profile or login with a previously created profile. " +
                "You only can start playing this game  after you logining");
        Text mode = new Text("There are 4 modes that each one indicates an unique dictionary " +
                "to select the target words for you playing. Each mode has 8 level. You need unlock advanced levels by" +
                "pass the previous level ");
        Text level = new Text("Once you decide a specific mode you want to play, " +
                "press the Start button. You will go to level selection screen");
        Text start = new Text("After you selection a level to play, you will go to the game play screen." +
                "At this time, the game dose not really start. You need to press the play button in the " +
                "middle bottom of the grids to really start. Once you press the play button, the timer will countdown," +
                "and the letter will appear in the grid");
        Text play = new Text("Now, you can select words by either typing or mouse drag." +
                "you can also pause and resume the game by press pasue/play the button in the middle bottom of the grids, " +
                "which is same bottom to the play button");
        Text rule = new Text("Once your score is equal or higher to the target point, or the remaining " +
                "time is 0, the game will stop,and you can neither typing nor mouse drag. If you win current level, you " +
                "can press next level button to play next level in the middle bottom of the grids, which is same bottom " +
                "to the play button. If you do not win current level, you need press the restart bottom to restart current" +
                "level, or you can go back to home screen to select other modes or levels.");
        login.setWrappingWidth(330);
        mode.setWrappingWidth(330);
        level.setWrappingWidth(330);
        start.setWrappingWidth(330);
        play.setWrappingWidth(330);
        rule.setWrappingWidth(330);
        information.add(new Text("Login:"),0,0);
        information.add(login,1,0);

        information.add(new Text("Mode:"),0,1);
        information.add(mode,1,1);
        information.add(new Text("Level:"),0,2);
        information.add(level,1,2);

        information.add(new Text("Start:"),0,3);
        information.add(start,1,3);
        information.add(new Text("Play:"),0,4);
        information.add(play,1,4);
        information.add(new Text("Rule:"),0,5);
        information.add(rule,1,5);
        help.setContent(information);

    }

}

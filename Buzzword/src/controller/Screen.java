package controller;

/**
 * Created by zhangxinyao on 12/10/16.
 */
public enum Screen {
    HOMESCREEN,
    LEVELSCREEN,
    PLAYSCREEN,
    PROFILESCREEN,
    HELPSCREEN
}

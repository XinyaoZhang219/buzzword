package controller;

import apptemplate.AppTemplate;
import data.Category;
import data.GameData;
import gui.EditDialog;
import gui.Workspace;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_IMAGEDIR_PATH;

/**
 * Created by zhangxinyao on 11/21/16.
 */
public class ProfileController {
    PropertyManager propertyManager = PropertyManager.getManager();

    private AppTemplate appTemplate;
    private BuzzwordController controller;
    private GameData gamedata;
    private Button     home= new Button("Home");

    private VBox playPane;
    private HBox info = new HBox(30);
    private Button editPw = new Button();
    public ProfileController(AppTemplate app){
        appTemplate = app;
    }

    public void handleProfileSetting(){
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
        controller = (BuzzwordController) appTemplate.getGUI().getFileController();
        VBox buttonPane = workspace.getButtonPane();
        controller.setCurrentScreen(Screen.PROFILESCREEN);
        home.setOnAction(e -> {
            controller.getHomeController().loginedHomeScreen();
        });
        home.setMaxWidth(Double.MAX_VALUE);
        buttonPane.getChildren().setAll(home);

        gamedata = (GameData) appTemplate.getDataComponent();
        playPane= workspace.getPlayPane();
        try {
            setButtonImage(editPw,EDIT_ICON.toString(),EDIT_TOOLTIP.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        informationShow();
        VBox playInfoPlan = workspace.getPlayInfoPane();
        playInfoPlan.getChildren().setAll();

    }

    private void informationShow(){

        info.setPadding(new Insets(50,0,0,50));
        VBox label = new VBox(10);
        VBox data = new VBox(10);
        VBox edit = new VBox(10);
        Text n = new Text("Name:");
        Text name = new Text(gamedata.getName());
        n.setId("information");
        name.setId("information");

        Text pw = new Text("Password:");
        Text password = new Text(gamedata.getPassword());
        pw.setId("information");
        password.setId("information");

        Text pl = new Text("Passed Level:");
        Text empty = new Text("  ");
        pl.setId("information");
        empty.setId("information");

        int[] passLevel = gamedata.getPasslevel();
        Text[][] text = new Text[4][2];
        text[0][0] = new Text(gamedata.categoryToString(Category.ENGLISH_DICTIONARY));
        text[0][1] = new Text(passLevel[Category.ENGLISH_DICTIONARY.getParameter()]+"");

        text[1][0] = new Text(gamedata.categoryToString(Category.DUTCH));
        text[1][1] = new Text(passLevel[Category.DUTCH.getParameter()]+"");


        text[2][0] = new Text(gamedata.categoryToString(Category.ITALIAN));
        text[2][1] = new Text(passLevel[Category.ITALIAN.getParameter()]+"");

        text[3][0] = new Text(gamedata.categoryToString(Category.NORWEGIAN));
        text[3][1] = new Text(passLevel[Category.NORWEGIAN.getParameter()]+"");
        for (int i=0;i<4;i++){
            for (int j=0;j<2;j++){
                text[i][j].setId("information");
            }
        }
        label.getChildren().setAll(n,pw,pl,text[0][0],text[1][0],text[2][0],text[3][0]);
        data.getChildren().setAll(name, password,empty,text[0][1],text[1][1],text[2][1],text[3][1]);
        edit.getChildren().setAll(new Text(" "),editPw);
        info.getChildren().setAll(label,data,edit);
        playPane.getChildren().setAll(info);
        setEditAction(data);

    }
    private void setEditAction(VBox data){
        editPw.setOnAction(e ->{
            EditDialog editDialog = EditDialog.getSingleton();
            editDialog.show(propertyManager.getPropertyValue(EDIT_TITLE),
                    propertyManager.getPropertyValue(EDIT_MESSAGE));
            if (editDialog.getSelection().equals(EditDialog.CONFIRM)){
                if (editDialog.getInputText().length()!=0){
                    gamedata.setPassword(editDialog.getInputText());
                    try {
                        controller.handleSaveRequest();
                    } catch (IOException e1) {
                        AppMessageDialogSingleton d = AppMessageDialogSingleton.getSingleton();
                        d.show(propertyManager.getPropertyValue(SAVE_ERROR_TITLE),
                                propertyManager.getPropertyValue(SAVE_ERROR_MESSAGE));
                    }
                }
                Text pw = (Text)data.getChildren().get(1);
                pw.setText(gamedata.getPassword());
            }
        });
    }
    private void setButtonImage(Button button,String icon, String tooltip) throws IOException {

        URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
        if (imgDirURL == null)
            throw new FileNotFoundException("Image resources folder does not exist.");

        try (InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(propertyManager.getPropertyValue(icon)))) {
            Image buttonImage = new Image(imgInputStream);
            ImageView buttonImageView = new ImageView(buttonImage);
            buttonImageView.setFitWidth(12);
            buttonImageView.setFitHeight(12);
            button.setGraphic(buttonImageView);
            Tooltip buttonTooltip = new Tooltip(propertyManager.getPropertyValue(tooltip));
            button.setTooltip(buttonTooltip);
        } catch (URISyntaxException e) {
            AppMessageDialogSingleton d = AppMessageDialogSingleton.getSingleton();
            d.show("Image Error","Can not find such image or file");
            System.exit(1);

        }

    }

}

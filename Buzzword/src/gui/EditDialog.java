package gui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.net.URL;

/**
 * @author Ritwik Banerjee
 */
public class EditDialog extends Stage {
    // HERE'S THE SINGLETON
    static EditDialog singleton;

    // GUI CONTROLS FOR OUR DIALOG
    VBox   messagePane;
    Scene  messageScene;
    Label  messageLabel;
    Button confirmButton;
    Button cancelButton;
    String selection;
    TextField input=new TextField();

    // CONSTANT CHOICES
    public static final String CONFIRM    = "Confirm";
    public static final String CANCEL = "Cancel";

    /**
     * Note that the constructor is private since it follows
     * the singleton design pattern.
     */
    private EditDialog() {}

    /**
     * The static accessor method for this singleton.
     *
     * @return The singleton object for this type.
     */
    public static EditDialog getSingleton() {
        if (singleton == null)
            singleton = new EditDialog();
        return singleton;
    }

    /**
     * This method initializes the singleton for use.
     *
     * @param primaryStage The window above which this dialog will be centered.
     */
    public void init(Stage primaryStage) {
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        this.initStyle(StageStyle.TRANSPARENT);

        // LABEL TO DISPLAY THE CUSTOM MESSAGE
        messageLabel = new Label();

        // YES, NO, AND CANCEL BUTTONS
        confirmButton = new Button(CONFIRM);
        cancelButton = new Button(CANCEL);

        // MAKE THE EVENT HANDLER FOR THESE BUTTONS
        EventHandler<ActionEvent> yesNoCancelHandler = event -> {
            EditDialog.this.selection = ((Button) event.getSource()).getText();
            EditDialog.this.hide();
        };


        // AND THEN REGISTER THEM TO RESPOND TO INTERACTIONS
        confirmButton.setOnAction(yesNoCancelHandler);
        cancelButton.setOnAction(yesNoCancelHandler);

        // NOW ORGANIZE OUR BUTTONS
        HBox buttonBox = new HBox(50);
        buttonBox.setAlignment(Pos.CENTER);
        buttonBox.getChildren().add(confirmButton);
        buttonBox.getChildren().add(cancelButton);

        // WE'LL PUT EVERYTHING HERE
        messagePane = new VBox();
        messagePane.setAlignment(Pos.CENTER);
        input.clear();
        messagePane.getChildren().addAll(messageLabel,input,buttonBox);
        // MAKE IT LOOK NICE
        messagePane.setPadding(new Insets(10, 20, 20, 20));
        messagePane.setSpacing(10);
        messagePane.setPrefHeight(150);
        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(messagePane);
        messageScene.setFill(null);
        this.setScene(messageScene);
        initStylesheet();
    }

    /**
     * Accessor method for getting the selection the user made.
     *
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }

    /**
     * This method loads a custom message into the label
     * then pops open the dialog.
     *
     * @param title   The title to appear in the dialog window bar.
     * @param message Message to appear inside the dialog.
     */
    public void show(String title, String message) {
        // SET THE DIALOG TITLE BAR TITLE
        setTitle(title);

        // SET THE MESSAGE TO DISPLAY TO THE USER
        messageLabel.setText(message);

        // AND OPEN UP THIS DIALOG, MAKING SURE THE APPLICATION
        // WAITS FOR IT TO BE RESOLVED BEFORE LETTING THE USER
        // DO MORE WORK.
        showAndWait();
    }
    public void initStylesheet() {
        URL cssResource = getClass().getClassLoader().getResource("css/dialog_style.css");
        assert cssResource != null;
        messageScene.getStylesheets().add(cssResource.toExternalForm());
    }
    public String getInputText(){return input.getText();}
}

package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.BuzzwordController;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import ui.AppGUI;

/**
 * Created by zhangxinyao on 11/9/16.
 *
 * @author Xinyao Zhang
 */
public class Workspace extends AppWorkspaceComponent{

    private AppTemplate app;
    private AppGUI      gui;
    private BuzzwordController controller;
    //heading
    private HBox               headPane = new HBox();
    private Label              headPaneLabel = new Label("!! Buzz Word !!");
    //body
    private BorderPane         bodyPane;
        //left side
        private VBox               buttonPane = new VBox(20);
        private Button             createNewPtofile = new Button("Create New Profile");
        private Button             login = new Button("Login");;

        //center
        private VBox               playPane = new VBox(20);
        private BorderPane         grid;
        private StackPane[][]      letterGridPane = new StackPane[4][4];
        private Circle[][]        letterGridCircle = new Circle[4][4];
        private Text[][]           word = new Text[4][4];



    //right
        private VBox               playInfoPane = new VBox(20);



    public VBox getButtonPane(){ return buttonPane; }
    public VBox getPlayPane(){ return playPane; }
    public VBox getPlayInfoPane(){ return playInfoPane;}
    public BorderPane getGrid(){ return grid;}
    public Text[][] getWord(){ return word;}



    public  Circle[][] getLetterGridCircle(){ return letterGridCircle;}
    public Workspace(AppTemplate appTemplate){
        app = appTemplate;
        gui = app.getGUI();
        controller = (BuzzwordController) gui.getFileController();
        initHomeScreen();
        controller.shortcut();
    }


    public void initHomeScreen(){
        headPaneLabel.setId("headLabel");
        headPane.getChildren().setAll(headPaneLabel);
        headPane.setAlignment(Pos.CENTER);
        bodyPane = new BorderPane();

        // left side contain buttons
        buttonPane.setPadding(new Insets(50,20,0,20));
        buttonPane.setPrefWidth(gui.getAppWindowWidth()/4);
        createNewPtofile.setMaxWidth(Double.MAX_VALUE);

        login.setMaxWidth(Double.MAX_VALUE);
        buttonPane.getChildren().setAll(createNewPtofile,login);
        bodyPane.setLeft(buttonPane);

        createNewPtofile.setOnAction(e -> controller.getHomeController().handleCreateNewProfile());
        login.setOnAction(e -> controller.getHomeController().handleLogin());
        //center pane contain grids and some label
        playPane.setAlignment(Pos.CENTER);

        grid = new BorderPane();
        initLetterGirdPane();
        buzzwordText();
        playPane.getChildren().setAll(grid);
        bodyPane.setCenter(playPane);

        //right side contain play information
        playInfoPane.setPrefWidth(gui.getAppWindowWidth()/4);
        playInfoPane.setPadding(new Insets(50,20,0,20));
        playInfoPane.getChildren().clear();
        bodyPane.setRight(playInfoPane);

        workspace.getChildren().setAll(headPane,bodyPane);

    }

    public void buzzwordText(){
        word[0][0].setText("B");
        word[0][1].setText("U");
        word[1][0].setText("Z");
        word[1][1].setText("Z");
        word[2][2].setText("W");
        word[2][3].setText("O");
        word[3][2].setText("R");
        word[3][3].setText("D");
    }

    public void initLetterGirdPane(){
        grid.setPrefSize(320,320);
        for (int i=0; i<4; i++) {
            for (int j = 0; j < 4; j++) {
                letterGridPane[i][j] = new StackPane();
                word[i][j] = new Text("");
                word[i][j].setId("letter");
                word[i][j].setMouseTransparent(true);
                letterGridPane[i][j].prefWidthProperty().bind(grid.widthProperty().divide(4));
                letterGridPane[i][j].prefHeightProperty().bind(grid.heightProperty().divide(4));
                letterGridPane[i][j].layoutXProperty().bind(grid.widthProperty().divide(4)
                        .multiply(j).add(grid.widthProperty().divide(8)));
                letterGridPane[i][j].layoutYProperty().bind(grid.heightProperty().divide(4)
                        .multiply(i).add(grid.heightProperty().divide(8)));
                letterGridCircle[i][j] = new Circle(25);
                letterGridCircle[i][j].setId("circle");
                letterGridPane[i][j].getChildren().setAll(letterGridCircle[i][j], word[i][j]);
                grid.getChildren().add(letterGridPane[i][j]);

            }
        }
    }


    @Override
    public void initStyle() {

    }

    @Override
    public void reloadWorkspace() {

    }

    public Pane getWorkspace(){ return workspace;}
}

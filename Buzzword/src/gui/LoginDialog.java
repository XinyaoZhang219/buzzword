package gui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.net.URL;

/**
 * Created by zhangxinyao on 11/10/16.
 * @author Xinyao Zhang
 */
public class LoginDialog extends Stage {

    private static LoginDialog singleton = null;
    private Scene   messageScene;
    private VBox messagePane;
    private TextField       account;
    private PasswordField   password;
    private Label error;


    private LoginDialog() { }

    public static LoginDialog getSingleton() {
        if (singleton == null)
            singleton = new LoginDialog();
        return singleton;
    }
    public void init(Stage owner) {
        initModality(Modality.WINDOW_MODAL); // modal => messages are blocked from reaching other windows
        initOwner(owner);
        this.initStyle(StageStyle.TRANSPARENT);
        // LABEL TO DISPLAY THE CUSTOM MESSAGE

        account = new TextField();
        password = new PasswordField();
        HBox errorPane = new HBox();
        error = new Label();
        error.setId("error");
        errorPane.getChildren().add(error);
        errorPane.setAlignment(Pos.CENTER_RIGHT);
        messagePane = new VBox(20);
        messagePane.setAlignment(Pos.CENTER_LEFT);
        messagePane.setPadding(new Insets(20, 20, 20, 20));

        GridPane loginPane = new GridPane();
        loginPane.setVgap(20);
        loginPane.setHgap(10);
        Label a = new Label("Account: ");
        loginPane.add(a,0,0);
        loginPane.add(account,1,0);
        Label l = new Label("Password: ");
        loginPane.add(l,0,1);
        loginPane.add(password,1,1);
        Label hint = new Label("Esc to cancel   Enter to confirm");
        messagePane.getChildren().addAll(hint,loginPane,errorPane);
        messageScene = new Scene(messagePane);
        messageScene.setFill(null);
        this.setScene(messageScene);
        initStylesheet();
    }
    public void show(String title) {
        setTitle(title); // set the dialog title
        showAndWait(); // opens the dialog, and waits for the user to resolve using one of the given choices
    }

    public TextField getAccount(){
        return account;
    }

    public PasswordField getPassword(){
        return password;
    }


    public void setError(String text, Boolean visible){
        error.setText(text);
        error.setVisible(visible);
    }

    private void initStylesheet() {
        URL cssResource = getClass().getClassLoader().getResource("css/login_style.css");
        assert cssResource != null;
        messageScene.getStylesheets().add(cssResource.toExternalForm());
    }

}
